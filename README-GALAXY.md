# Ansible Collection - c2platform.test

[![Pipeline Status](https://gitlab.com/c2platform/ansible-collection-test/badges/master/pipeline.svg?style=flat-square&key_text=Pipeline+Status&key_width=90)](https://gitlab.com/c2platform/ansible-collection-test/-/pipelines) [![Latest Release](https://gitlab.com/c2platform/ansible-collection-test/-/badges/release.svg?style=flat-square)](https://gitlab.com/c2platform/ansible-collection-test/-/pipelines)

See full [README](https://gitlab.com/c2platform/ansible-collection-test/-/blob/master/README.md).