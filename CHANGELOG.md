# CHANGELOG

## 1.0.0 ( 2023-06-16 )

* New pipeline script.
* [microk8s](./roles/microk8s/README.md) new role.
